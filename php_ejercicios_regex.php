﻿//Realizar una expresión regular que detecte emails correctos.
/^[A-Za-z]+@[a-z]+\.[a-z]+*$/


//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
"/^[a-zA-Z0-9_]*$/"


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
/^((A-Za-z){50,}\s*)$/


//Crea una funcion para escapar los simbolos especiales.
$nuevaCadena = preg_replace('/[^#$%&@<>?~]/i', '_', $unir);

//Crear una expresion regular para detectar números decimales.
/^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/